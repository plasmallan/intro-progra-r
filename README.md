# Introducción a la Programación con R

Repositorio con los contenidos recibidos y generados en el curso de Introducción a la Programación con R del CNCA del CeNAT de Costa Rica. El curso buscó cubrir desde temas relacionados con tipos de datos, hasta la manipulación de estos y los dataframes.